package authenticators;

import org.jboss.logging.Logger;
import org.keycloak.authentication.*;

import org.keycloak.authentication.requiredactions.UpdatePassword;
import org.keycloak.models.*;



import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;

public class MyAuthenticator implements Authenticator {


    private static final org.jboss.logging.Logger log = Logger.getLogger(MyAuthenticator.class);





    @Override
    public void authenticate(AuthenticationFlowContext context) {
        context.success();
       // Response challenge=context.form().createForm("secret-question.ftl");
       // context.challenge(challenge);

    }

    @Override
    public void action(AuthenticationFlowContext context) {
        boolean validated = validateAnswer(context);
        if (!validated) {
            Response response =  context.form()
                    .setError("badSecret")
                    .createForm("secret-question.ftl");
            context.failureChallenge(AuthenticationFlowError.INVALID_CREDENTIALS, response);
            return;
        }

        context.success();
    }

    @Override
    public List<RequiredActionFactory> getRequiredActions(KeycloakSession session) {

        return Collections.singletonList((UpdatePassword)session.getKeycloakSessionFactory().getProviderFactory(RequiredActionProvider.class,UserModel.RequiredAction.UPDATE_PASSWORD.name()));
    }

    protected boolean validateAnswer(AuthenticationFlowContext context) {


        return true;
    }


    @Override
    public boolean requiresUser() {

        return true;
    }

    @Override
    public boolean configuredFor(KeycloakSession session, RealmModel realm, UserModel user) {



        return false;
    }


    @Override
    public void setRequiredActions(KeycloakSession session, RealmModel realm, UserModel user) {
        user.addRequiredAction(UserModel.RequiredAction.UPDATE_PASSWORD);
        //user.addRequiredAction(UserModel.RequiredAction.CONFIGURE_TOTP);


    }

    @Override
    public void close() {

    }
}
