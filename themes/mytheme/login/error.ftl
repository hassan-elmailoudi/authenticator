<#import "template.ftl" as layout>
<@layout.registrationLayout displayMessage=false; section>
    <#if section = "header">
            <img id="img" src="${url.resourcesPath}/img/Meteosol-Logo.jpg">
            </br>
            </br>
            </br>
    <h2 id="errorTitle">
        ${msg("errorTitle")}
    </h2>
    <#elseif section = "form">
        <div id="kc-error-message">
            <div class="light-grey-text">${message.summary}</div>
            <#if client?? && client.baseUrl?has_content>
                <div class="error-message-margin light-grey-text"><a id="backToApplication" href="${client.baseUrl}">${msg("backToApplication")?no_esc}</a></div>
            </#if>
        </div>
    </#if>
</@layout.registrationLayout>