<#import "template.ftl" as layout>

<@layout.registrationLayout displayInfo=true displayMessage=!messagesPerField.existsError('username'); section>
    <#if section = "header">
        <img id="img" src="${url.resourcesPath}/img/Meteosol-Logo.jpg">
    <#elseif section = "form">
        </br>
        </br>
        </br>
        <form id="kc-reset-password-form" class="${properties.kcFormClass!}" action="${url.loginAction}" method="post">
            <div class="${properties.kcFormGroupClass!}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="username" class="${properties.kcLabelClass!}">
                        <#if !realm.loginWithEmailAllowed>
                            ${msg("username")}
                        <#elseif !realm.registrationEmailAsUsername>
                            ${msg("usernameOrEmail")}<#else>${msg("email")}
                        </#if>
                    </label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="username" name="username" class="${properties.kcInputClass!}"
                    autofocus/>
                    <#if messagesPerField.existsError('username')>
                         <span         id="input-error-username"
                                    class="${properties.kcInputErrorMessageClass!}"
                                aria-live="polite"
                                >
                           ${kcSanitize(messagesPerField.get('username'))?no_esc}
                         </span>
                    </#if>
                </div>
                <br/>
            </div>

            <div class="${properties.kcFormGroupClass!} m-0">
                <br/>
                <input id="resetPswdSubmitButton"
                    class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!}
                           ${properties.kcButtonBlockClass!} ${properties.kcButtonLargeClass!} m-0"
                    type="submit"
                    value="${msg("resetPswdSubmit")}"
                />
                <br/>
            </div>

            <div class="${properties.kcFormGroupClass!} password-forgotten-back-to-start-spacing">
                 <div class="backToLoginFromForgotPswd">
                      <a id="backToLoginFromForgotPswdLink" href="${url.loginUrl}" class="">
                          ${msg("backToLoginFromForgotPswd")?no_esc}
                      </a>
                 </div>
                 <br/>
            </div>

        </form>
    <#elseif section = "info" >
    </#if>
</@layout.registrationLayout>