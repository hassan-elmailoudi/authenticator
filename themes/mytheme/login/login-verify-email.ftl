<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=true; section>
    <#if section = "header">
       <img id="img" src="${url.resourcesPath}/img/Meteosol-Logo.jpg">
       </br>
       </br>
       </br>
       <h2 id="emailVerifyTitle">
          ${msg("emailVerifyTitle")}
       </h2>
    <#elseif section = "form">
        <p id="emailVerifyInstruction" class="instruction">${msg("emailVerifyInstruction1")}</p>
    <#elseif section = "info">
        <p id="emailVerifyInstruction" class="instruction">
            ${msg("emailVerifyInstruction2")}
            <br/>
            <a href="${url.loginAction}">
                ${msg("doClickHere")}
            </a>
            ${msg("emailVerifyInstruction3")}
        </p>
    </#if>
</@layout.registrationLayout>